# tune и docker-compose v2

Пример запуска сервиса и tuna с помощью docker-compose.

Обратите внимание, что значение переменной окружения `TUNA_TOKEN` (файл `compose.yaml`) должно быть таким же, как в [профиле](https://my.tuna.am/#!/profile).

## Ссылки

* https://tuna.am/docs/examples/docker
