# tuna и Kubernetes

Пример запуска сервиса и tuna в Kubernetes.

Обратите внимание, что значение переменной окружения `TUNA_TOKEN` (файл `tuna/deployment.yaml`) должно быть таким же, как в [профиле](https://my.tuna.am/#!/profile).

## Ссылки

* https://tuna.am/docs/examples/kubernetes
